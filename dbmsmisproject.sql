/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.1.37 : Database - dbmsmisproject
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbmsmisproject` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbmsmisproject`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `uname` varchar(40) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `contactno` varchar(30) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  PRIMARY KEY (`uname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`uname`,`pass`,`fname`,`lname`,`contactno`,`gender`) values ('admin','admin','yub','basnet','9808390957','Male'),('basnetyub','helloworld','Yub','Basnet','9808390957','Male'),('maharjananil','anilmaharjan','Anil','Maharjan','9849205589','Male'),('pj','asdf','Prajwal','Gurun','34567','Male');

/*Table structure for table `studentinfo` */

DROP TABLE IF EXISTS `studentinfo`;

CREATE TABLE `studentinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `mname` varchar(30) DEFAULT NULL,
  `lname` varchar(30) NOT NULL,
  `department` varchar(30) NOT NULL,
  `semester` varchar(30) NOT NULL,
  `contactno` varchar(15) DEFAULT NULL,
  `dateofentry` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `studentinfo` */

insert  into `studentinfo`(`id`,`fname`,`mname`,`lname`,`department`,`semester`,`contactno`,`dateofentry`) values (7,'Yub','Raj','Basnet','it','6','9039493939','2013-02-03'),(11,'Urendra','','Khakurel','csit','5','9089444433','2015-12-06'),(13,'abc','abc','abc','acb','5','2342353','2016-08-12'),(15,'yub','basnet','basnet','bscit','5th ','394830438','2016-09-14');

/*Table structure for table `teacherinfo` */

DROP TABLE IF EXISTS `teacherinfo`;

CREATE TABLE `teacherinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `mname` varchar(30) DEFAULT NULL,
  `lname` varchar(30) NOT NULL,
  `department` varchar(30) NOT NULL,
  `salary` float NOT NULL,
  `contactno` varchar(20) DEFAULT NULL,
  `dateofentry` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `teacherinfo` */

insert  into `teacherinfo`(`id`,`fname`,`mname`,`lname`,`department`,`salary`,`contactno`,`dateofentry`) values (1,'Yub','','Basnet','it',10000,'9049383838','2034-04-03'),(2,'Anil','','Maharjan','IT',50000,'9843293478','2015-11-26'),(3,'Utsav','','Luitel','it',2000,'9095650656','2015-12-04');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
