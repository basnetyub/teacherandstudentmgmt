package dbmsproject;

import java.util.Calendar;
import java.text.SimpleDateFormat;

public class TestDate {
  public String getdate(){
    String DATE_FORMAT = "yyyy-MM-dd";
    SimpleDateFormat sdf =
          new SimpleDateFormat(DATE_FORMAT);
    Calendar c1 = Calendar.getInstance(); // today
    return(sdf.format(c1.getTime()));
  }
}