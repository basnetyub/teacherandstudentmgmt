package dbmsproject.forteacher;

import dbmsproject.forstudent.*;
import dbmsproject.jdbc_Connection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

public class SingletonThread {

    private static SingletonThread st = null;

    public static SingletonThread getInstance() {
        if (st == null) {
            synchronized (SingletonThread.class) {
                if (st == null) {
                    st = new SingletonThread();
                }
            }
        }
        return st;
    }

    public void getTableDatas(teajframe frame) {
        teajframe f = frame;
        String sqlSelect = "Select * from studentinfo";
        DefaultTableModel model = (DefaultTableModel) f.jTable1.getModel();
        model.setRowCount(0);
        try {
            Connection con = jdbc_Connection.getConnection();
            PreparedStatement pstat = con.prepareStatement(sqlSelect);
            ResultSet rs = pstat.executeQuery();
            while (rs.next()) {
                model.addRow(new Object[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8)
                });
            }
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
}
